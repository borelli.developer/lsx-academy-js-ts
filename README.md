
1. Nella barra laterale sinistra di VS Code, selezionare "Estensions"
2. Cercare ed installare l'estensione VS Code: "Live Server"
3. Lanciare il comando "npm install" nel terminale a livello root di progetto
4. Tasto destro sul file "src/index.html" e selezionare la voce "Open with Live Server"
5. Nel browser aprire la barra di ispezione elemento: Tasto destro sulla pagina -> Ispeziona oppure premere F12
6. Se tutto è andato a buon fine, nella tab "console" si dovrebbe vedere il messaggio "Hello World!"


---

Per generare i dati: https://www.mockaroo.com/
