
const a = "5";
const b = +a;
const currentDate = new Date();
//console.log(a + 3);
//console.log(b + 3);

/*
let pippo = {
    name: "Mario", 
    tutor: {
        name: "asd"
    },
    somma: function(a, b) {
        return a + b;
    },

    somma2: (a, b) => {
        return a + b; 
    }
};

pippo = undefined;
*/
/*
function Studente(name, eta) {
    this.name = name;
    this.eta = eta;
    this.materieApprese = [];
    this.apprendi = (materia, cb) => {
        this.materieApprese.push(materia);
        if(cb && typeof cb === 'function') {
            cb();
        }
        
    }
}



let pippo = new Studente("Marcellino", 28);


if(pippo?.name) {
    pippo.apprendi("Storia");

    pippo.apprendi("Italiano", () => {
        console.log("Adesso vado a studiare altro!");
        pippo.apprendi("Informatica");
    });

    // ["Storia", "Italiano", "Informatica"] -> ["Oggi ho appreso Storia", "Oggi ho appreso Italiano", "Oggi ho appreso Informatica"]
    // ["Storia", "Italiano", "Informatica"] -> ["Oggi ho appreso Storia", "Oggi ho appreso Italiano"]

    pippo.materieApprese
                .map((materia) => {
                    return "Oggi ho appreso " + materia;
                })
                .forEach((message) => {
                    console.log(message);
                });
    
    pippo.materieApprese = pippo.materieApprese.map((materia) => {
        return "Oggi ho appreso " + materia;
    });
    
    console.log(pippo.materieApprese)



} else {
    console.log("NON è valorizzato!");
}


function MateriaAppresa(materia, dataApprendimento = new Date()) {
    this.materia = materia;
    this.dataApprendimento = dataApprendimento;
}


let materieApprese = [];
const storia = new MateriaAppresa("Storia", new Date(2022, 9, 3));
materieApprese.push(storia);
materieApprese.push(new MateriaAppresa("Italiano", new Date(2022,9,5)));
materieApprese.push(new MateriaAppresa("Informatica"));

console.log(materieApprese);

let a2 = {
    name: "pippo"
};
const b2 = {
    name: "pippo"
};
console.log(a2 === b2)

/*

let materieOdierne = materieApprese.filter(materiaAppresa => materiaAppresa.dataApprendimento.toDateString() === new Date().toDateString())
                                    .map(materiaAppresaOggi => materiaAppresaOggi.materia)
                                    .join(', ')

console.log(materieOdierne)


let dataOggi = new Date();
materieApprese.filter(materiaAppresa => materiaAppresa.dataApprendimento.getUTCDate() === dataOggi.getUTCDate() &&
                        materiaAppresa.dataApprendimento.getUTCMonth() === dataOggi.getUTCMonth() &&
                        materiaAppresa.dataApprendimento.getUTCFullYear() === dataOggi.getUTCFullYear());

*/

// ["Storia", "Italiano", "Informatica"] -> ["Oggi ho appreso Storia", "Oggi ho appreso Italiano"]
/*
materieApprese = materieApprese.map(materia => "Oggi ho appreso " + materia)
                                .filter(materia => !materia.includes("Informatica"))
*/                              
// Prima filter, poi map
// 1. ["Storia", "Italiano", "Informatica"] -> ["Storia", "Italiano"]
// 2. ["Oggi ho appreso Storia", "Oggi ho appreso Italiano"]


// Prima map, poi filter
// 1. ["Storia", "Italiano", "Informatica"] -> ["Oggi ho appreso Storia", "Oggi ho appreso Italiano", "Oggi ho appreso Informatica"]
// 2. ["Oggi ho appreso Storia", "Oggi ho appreso Italiano", "Oggi ho appreso Informatica"] -> ["Oggi ho appreso Storia", "Oggi ho appreso Italiano"]

            
/*
const qualcunaDiIndirizzo = materieApprese.some(materiaAppresa => { 
    return materiaAppresa.materia === "Informatica";
});

console.log(qualcunaDiIndirizzo);
*/




/*
const somma2 = (a, b) => {
    return a + b;
}

function somma(a, b) {
    return a + b;
}



somma(a,b);
somma2(a,b);

*/

/*
// MAP

const map = new Map();
map.set(1, "Pippo");
map.set("Paolo", {name: "Maria"});




const objMap = {};
objMap[1321321] = false;
objMap[321] = "Marcellino";


objMap.Paolo = {name: "Maria"};

[212,1321321,321,32,312].filter(id => objMap[id] !== undefined && objMap[id] !== null).forEach(id => {    
    console.log(objMap[id]);
});

const asJson = JSON.stringify(objMap);
console.log(asJson);

const toObj = JSON.parse(asJson);
console.log("->",toObj[321]);

/*
    {
        14332423: "Pippo",
        Paolo: {name: "Maria"}
    }
*/




// SET

let materieStudiate = [{
    giorno: new Date(),
    materiaStudiata: 'Italiano'
},
{
    giorno: new Date(2022, 9, 6),
    materiaStudiata: 'Italiano'
},
{
    giorno: new Date(2022, 9, 7),
    materiaStudiata: 'Francese'
},
{
    giorno: new Date(2022, 9, 8),
    materiaStudiata: 'Informatica'
},
{
    giorno: new Date(2022, 9, 9),
    materiaStudiata: 'Matematica'
}].map(giornoDiStudio => giornoDiStudio.materiaStudiata);

materieStudiate = Array.from(new Set(materieStudiate)).join(', ');

console.log(materieStudiate)


const original = ["Andrea","Andrea", "Luca", "Luca"];
const studenti = new Set(["Andrea", "Luca", "Luca"]);
console.log(Array.from(new Set(original)));




console.log([2, 3, 7].map(value => value**2));

console.log(["Mario", "Luca", "Andrea"].map(nome => "Ciao " + nome));

console.log(["Mario", "Luca", "Andrea"].map(nome => { 
    return "Ciao " + nome 
}));



/*
// Promise
// Macrotask -> Sincrono -> Microtask -> Macrotask -> ... 

const operazioneAsincrona = new Promise((resolve, reject) => {
    // ...
    if(Math.random() > .5) {
        resolve("Successo!");
    } else {
        reject("Fallimento!")
    }
});

operazioneAsincrona.then(value => {
    console.log(value);
}).catch(error => console.error(error));

console.log("1");
    

Promise.all([
    fetch('assets/students.json'),
    fetch('assets/students.json')
]).then((responses) => {
    console.log(responses);
    return responses[0].json()
})
.then(students => { 
    console.log("2");
    console.log(students);
});

/*
fetch('assets/students.json').then(response => response.json())
                            .then(students => { 
                                console.log("2");
                                console.log(students);
                            });
console.log("3");
*/









